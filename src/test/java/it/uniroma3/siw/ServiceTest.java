package it.uniroma3.siw;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.service.FotografoService;

@SpringBootTest
public class ServiceTest {
	
	
	
	
	private FotografoService frs = new FotografoService();
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void saveTest() {
		Fotografo franco = new Fotografo("franco","fontana");
		Fotografo iris = new Fotografo("iris","Gentili");
		frs.save(franco);
		frs.save(iris);
		assertEquals(frs.count(),2);
	}
	

}
