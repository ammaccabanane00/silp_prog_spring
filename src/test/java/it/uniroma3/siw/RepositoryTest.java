package it.uniroma3.siw;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.repository.FotografoRepository;

@SpringBootTest
class RepositoryTest {
	
	private FotografoRepository repoFotografo;
	private Fotografo franco;
	
	
	
	@Test
	void testPersistAndExist() {
		franco = new Fotografo("franco","fontana");
		//this.repoFotografo.save(franco);
		assertTrue(this.repoFotografo.existsById(franco.getId()));
	}

}
