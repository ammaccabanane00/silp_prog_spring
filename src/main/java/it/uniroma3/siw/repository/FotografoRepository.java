package it.uniroma3.siw.repository;

import it.uniroma3.siw.model.Fotografo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface FotografoRepository  extends CrudRepository<Fotografo, Long> {
	
	public List<Fotografo> findByfirstName(String firstName);
	
	public List<Fotografo> findByLastName(String lastName);
	
	public List<Fotografo> findByfirstNameAndLastName(String name, String lastName);

}
