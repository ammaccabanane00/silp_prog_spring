package it.uniroma3.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.model.Album;
import it.uniroma3.siw.model.Fotografo;

public interface AlbumRepository extends CrudRepository<Album, Long>{
	
	public List<Album> findByTitle(String titolo);
	
	public List<Album> findByAuthor(Fotografo autore);
	
	public List<Album> findByAuthorId(Long authorId);

}
