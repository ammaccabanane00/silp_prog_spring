package it.uniroma3.siw.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.siw.model.Foto;
import it.uniroma3.siw.model.Tag;

public interface FotoRepository extends CrudRepository<Foto, Long> {
	
	public List<Foto> findByTag(Tag tag);

}
