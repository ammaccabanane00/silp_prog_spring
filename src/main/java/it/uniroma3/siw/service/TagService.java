package it.uniroma3.siw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.model.Foto;
import it.uniroma3.siw.model.Tag;
import it.uniroma3.siw.repository.TagRepository;

@Transactional
@Service
public class TagService {
	
	@Autowired
	private TagRepository repo;
	
	public void addTag(Tag tag) {
		this.repo.save(tag);
	}

	public void rimuoviTag(Tag tag) {
		this.repo.delete(tag);
	}
	
	//OTHER
	
	public Long count() {
		return this.repo.count();
	}
	
	public List<Foto> getTaggedPic(Long tagId){
		return this.repo.findById(tagId).get().getPic();
	}

}
