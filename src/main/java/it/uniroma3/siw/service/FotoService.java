package it.uniroma3.siw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.model.Foto;
import it.uniroma3.siw.model.Tag;
import it.uniroma3.siw.repository.FotoRepository;

@Transactional
@Service
public class FotoService {
	
	@Autowired
	private FotoRepository repo;
	
	public void addFoto(Foto foto) {
		this.repo.save(foto);
	}
	
	public Foto findById(Long id) {
		return this.repo.findById(id).get();
	}
	
	public List<Foto> findByTag(Tag tag){
		return this.repo.findByTag(tag);
	}
	
	public void rimuoviFoto(Foto foto) {
		this.repo.delete(foto);
	}
	
	//OTHER
	
	public Long count() {
		return this.repo.count();
	}
	
	public byte[] getByteImage(Long picId) {
		return this.repo.findById(picId).get().getByteImage();
	}
	
	public String getAuthorFullName(Long picId) {
		String name = this.repo.findById(picId).get().getAuthor().getFirstName();
		String lastName = this.repo.findById(picId).get().getAuthor().getLastName();
		return (name + " " +lastName);
	}
	
	public List<Tag> getPicTag(Long picId) {
		return this.repo.findById(picId).get().getTag();
	}
	

}
