package it.uniroma3.siw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.model.Album;
import it.uniroma3.siw.model.Foto;
import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.repository.AlbumRepository;
import it.uniroma3.siw.repository.FotoRepository;

@Transactional
@Service
public class AlbumService {
	
	@Autowired
	private AlbumRepository repo;
	@Autowired
	private FotoRepository repoFoto;
	
	public Album findById(Long id) {
		return this.repo.findById(id).get();
	}
	
	public List<Album> allAlbum(){
		return (List<Album>)this.repo.findAll();
	}
	
	public List<Album> findByTitle(String title){
		return this.repo.findByTitle(title);
	}
	
	public List<Album> findByAuthor(Fotografo author){
		return this.repo.findByAuthor(author);
	}
	
	public List<Album> findByAuthorId(Long authorId){
		return this.repo.findByAuthorId(authorId);
	}
	
	public void removeAlbum(Album album) {
		this.repo.delete(album);
	}
	
	//OTHER 
	
	public Long count() {
		return this.repo.count();
	}
	
	public void changeTitle(Long albumId, String newTitle) {
		Album toChange = this.repo.findById(albumId).get();
		toChange.setTitle(newTitle);
		this.repo.save(toChange);
	}
	
	public void addPictureToAlbum(Long albumId, Long picId) {
		this.repo.findById(albumId).get().addFoto(this.repoFoto.findById(picId).get());
		this.repo.save(this.repo.findById(albumId).get());
	}
	
	public String getAlbumTitle(Long albumId) {
		return this.repo.findById(albumId).get().getTitle();
	}
	
	public List<Foto> getAlbumPictures(Long albumId) {
		return this.repo.findById(albumId).get().getPic();
	}
	
	public Fotografo getAuthor(Long albumId) {
		return this.repo.findById(albumId).get().getAuthor();
	}
	
	public void removePicFromAlbum(Long albumId, Long picId) {
		Album album = this.repo.findById(albumId).get();
		Foto pic = this.repoFoto.findById(picId).get();
		album.removeFoto(pic);
		this.repo.save(album);
	}
	
	public void removeAllPicsFromAlbum(Long albumId) {
		Album album = this.repo.findById(albumId).get();
		album.removeAlFotos();
		this.repo.save(album);
	}

}
