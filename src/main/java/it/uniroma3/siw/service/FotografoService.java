package it.uniroma3.siw.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.siw.model.Album;
import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.repository.AlbumRepository;
import it.uniroma3.siw.repository.FotografoRepository;

@Transactional
@Service
public class FotografoService {
	
	@Autowired
	private FotografoRepository repo;
	@Autowired
	private AlbumRepository repoAlbum;
	
	public void save(Fotografo fotografo) {
		this.repo.save(fotografo);
	}
	
	public List<Fotografo> allFotografi(){
		return (List<Fotografo>)this.repo.findAll();
	}

	public List<Fotografo> findByName(String name){
		return this.repo.findByfirstName(name);
	}

	public List<Fotografo> findByLastName(String lastName){
		return this.repo.findByLastName(lastName);
	}

	public List<Fotografo> findByNameAndLastName(String name, String lastName){
		return this.repo.findByfirstNameAndLastName(name, lastName);
	}
	
	public Fotografo findById(Long id) {
		return this.repo.findById(id).get();
	}
	
	public void rimuoviFotografo(Fotografo fotografo) {
		this.repo.delete(fotografo);
	}
	
	//OTHER
	public Long count() {
		return this.repo.count();
	}
	
	public void addAlbumToFotografo( Album album, Long fotografoId ) {
		Optional<Fotografo> fotografo = this.repo.findById(fotografoId);
		if (fotografo.isPresent()) {
			this.repoAlbum.save(album);
			this.repo.save(fotografo.get());
		}
	}
	
	public byte[] getFotografoPic(Long fotografoId) {
		Fotografo fotografo = this.repo.findById(fotografoId).get();
		return fotografo.getByteImage();
	}
	
	public List<Album> getFotografoAlbum(Long fotografoId){
		return this.repo.findById(fotografoId).get().getAlbum();	
	}
	
	public String getFotografoName(Long fotografoId) { 
		return this.repo.findById(fotografoId).get().getFirstName();
	}
	
	public String getFotografoLastName(Long fotografoId) {
		return this.repo.findById(fotografoId).get().getLastName();
	}
	
	public String getFotografoFullName(Long fotografoId) {
		
		String name = this.repo.findById(fotografoId).get().getFirstName();
		String lastName = this.repo.findById(fotografoId).get().getLastName();
		return (name + " " + lastName);
	}
	
	public List<Fotografo> getFotografoList(){
		return (List<Fotografo>)this.repo.findAll();
	}
	
	public void deleteAllFotografoAlbum(Long fotografoId) {
		this.repoAlbum.deleteAll(this.findById(fotografoId).getAlbum());
	}

}
