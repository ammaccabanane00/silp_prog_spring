package it.uniroma3.siw.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "album")
@NamedQuery(name="Album.findAll", query="SELECT album FROM Album album") 
public class Album {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
	private Long id;
	
	private String title;
	
	@ManyToOne
	private Fotografo author;
	
	@OneToMany(cascade = {CascadeType.REMOVE} )
	@JoinColumn( name = "pictures")
	private List<Foto> pictures;
	
	public Album() {
		pictures = new ArrayList<Foto>();
	}
	
	public Album(Fotografo autore, String nome){
		this();
		this.author = autore;
		this.title = nome;
	}
	
	public Long getId() {
		return id;
	}
	
	protected void setId(Long id) {
		
	}
	
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Fotografo getAuthor() {
		return author;
	}

	public void setAuthor(Fotografo author) {
		this.author = author;
	}
	
	public List<Foto> getPic(){
		return this.pictures;
	}
	
	protected void setPic(List<Foto> pic) {
		this.pictures = pic;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	//ToCheck
	public void addFoto(Foto foto) {
		this.pictures.add(foto);
	}
	
	public void removeFoto(Foto foto) {
		this.pictures.remove(foto);
	}
	
	public void removeAlFotos() {
		this.pictures.clear();
	}

}
