package it.uniroma3.siw.model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.NamedQuery;

@Entity

@Table(name = "fotografo" , uniqueConstraints=@UniqueConstraint (columnNames = { "firstname" , "lastname" } ) )
@NamedQuery(name="Fotografo.findAll", query="SELECT fotografo FROM Fotografo fotografo") 
public class Fotografo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "firstName")
	private String firstName;

	private String lastName;

	@OneToMany( mappedBy = "author" , fetch = FetchType.EAGER , cascade = {CascadeType.REMOVE})
	private List<Album> album;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] profilePic;

	public Fotografo() {
		this.album = new ArrayList<Album>();
	}

	public Fotografo(String nome, String cognome) {
		this();
		this.firstName = nome.toLowerCase();
		this.lastName = cognome.toLowerCase();
	}

	public Long getId() {
		return this.id;
	}

	//this can be useful in case of merge 
	protected void setId(Long id) {
		this.id=id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	protected void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<Album> getAlbum() {
		return album;
	}

	protected void setAlbum(List<Album> album) {
		this.album = album;
	}

	public byte[] getByteImage() {
		return this.profilePic;
	}

	public void setImage(String file) {
		File pic = new File(file);
		byte[] picInBytes = new byte[(int) pic.length()];
		this.profilePic = picInBytes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fotografo other = (Fotografo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	//ToCheck
	public void addAlbum( Album album ) {
		this.album.add(album);
	}

}
