package it.uniroma3.siw.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "foto")
@NamedQuery(name="Foto.findAll", query="SELECT foto FROM Foto foto") 
public class Foto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id" , nullable = false)
	private Long id;

	@ManyToOne
	private Fotografo author;

	@ManyToMany
	private List<Tag> tag;

	@Temporal( TemporalType.TIMESTAMP )
	private Date additionDate;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] foto;

	public Foto() {
		tag = new ArrayList<>();
	}

	public Foto( Fotografo autore , String file ){
		this();
		this.author = autore;
		this.setImage(file);;
	}

	public Long getId(){
		return this.id;
	}

	protected void setId(Long id) {
		this.id = id;
	}

	public Fotografo getAuthor() {
		return this.author;
	}

	protected void setAuthor(Fotografo author) {
		this.author = author;
	}

	public List<Tag> getTag(){
		return this.tag;
	}

	protected void setTag(List<Tag> tag) {
		this.tag = tag;
	}

	public byte[] getByteImage() {
		return this.foto;
	}

	public void setImage(String file) {
		File pic = new File(file);
		byte[] picInBytes = new byte[(int) pic.length()];
		this.foto = picInBytes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Foto other = (Foto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	//ToCheck
	public void addTag(Tag tag) {
		this.tag.add(tag);
	}
	
}

