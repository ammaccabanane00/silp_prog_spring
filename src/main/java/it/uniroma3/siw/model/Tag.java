package it.uniroma3.siw.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

@Entity
@NamedQuery(name="Tag.findAll", query="SELECT tag FROM Tag tag")
@Table(name = "tag")
public class Tag {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	private String tag;

	@ManyToMany(mappedBy = "tag")
	private List<Foto> pictures;

	public Tag() {
		pictures = new ArrayList<>();
	}
	
	public Tag(String tag){
		this();
		this.tag = tag;
	}

	public Long getId() {
		return this.id;
	}

	public String getTag() {
		return this.tag;
	}

	protected void setTag(String tag) {
		this.tag = tag;
	}

	public List<Foto> getPic(){
		return this.pictures;
	}

	protected void setPitures(List<Foto> pictures) {
		this.pictures = pictures;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tag == null) {
			if (other.tag != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		return true;
	}

	//ToCheck
	public void addPic(Foto pic) {
		this.pictures.add(pic);
	}
	
}