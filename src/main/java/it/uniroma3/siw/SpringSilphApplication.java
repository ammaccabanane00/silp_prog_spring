package it.uniroma3.siw;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.siw.model.Album;
import it.uniroma3.siw.model.Fotografo;
import it.uniroma3.siw.service.FotografoService;

@SpringBootApplication
//@ComponentScan("it.uniroma3.siw.service.FotografoService")
public class SpringSilphApplication {
	
	@Autowired
	private FotografoService fotografoService;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringSilphApplication.class, args);
	}
	
	@PostConstruct
	public void init() {
		
		Fotografo franco = new Fotografo("franco" , "fontana");
		fotografoService.save(franco);
		Fotografo robert = new Fotografo("robert" , "capa");
		fotografoService.save(robert);
		Fotografo steve = new Fotografo("steve" , "mccurry");
		fotografoService.save(steve);
		
		String a = "ci";
		
		for (int x = 0; x<10; x++) {
			Album primo = new Album(franco, a  ); 
			fotografoService.addAlbumToFotografo(primo, franco.getId());
			a= a+"ci";
		}
		for (int x = 0; x<3; x++) {
			Album primo = new Album(robert, "a" ); 
			fotografoService.addAlbumToFotografo(primo, robert.getId());
			a= a+"ci";
		}
		for (int x = 0; x<15; x++) {
			Album primo = new Album(steve, "a" ); 
			fotografoService.addAlbumToFotografo(primo, franco.getId());
			a= a+"ci";
		}
		
	}

}
