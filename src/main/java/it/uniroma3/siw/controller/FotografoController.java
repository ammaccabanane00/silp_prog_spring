package it.uniroma3.siw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import it.uniroma3.siw.service.FotografoService;

@Controller
public class FotografoController {
	
	@Autowired
	private FotografoService fotografoService;
	
	@RequestMapping(value = "/home")
	public String home( Model model ) {
		model.addAttribute("fotografi", this.fotografoService.allFotografi());
		model.addAttribute("ao",this.fotografoService.allFotografi().get(0).getFirstName());
		return "Index";
	}
	
	@RequestMapping( value = "/home/{fotografoId}")
	public String getAlbumAutore(@PathVariable("fotografoId")Long id, Model model) {
		model.addAttribute("albums", this.fotografoService.getFotografoAlbum(id));
		model.addAttribute("author",this.fotografoService.findById(id) );
		return "AlbumView";
	}
	
}
